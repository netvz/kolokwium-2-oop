package com.example.k2_client;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class HelloController {

    public HelloController(Merger mergeringer){
        mergeringer.setHelloController(this);
    }

    @FXML
    private Label wordCountLabel;

    @FXML
    private ListView wordList;

    @FXML
    private TextField filterField;

    @FXML
    protected void setWordCountLabel(int numberOfWords) {
        wordCountLabel.setText(String.valueOf(numberOfWords));
    }

    @FXML
    protected void setWordList(List<String> list){
        if(filterField.getText().isEmpty()){
            ObservableList<String> observableList  = FXCollections.observableList(list);
            wordList.setItems(observableList);
        }else{
            String searchCue = filterField.getText();
            List<String> filteredWords = list
                    .stream()
                    .filter(x -> x.split (" ")[1].startsWith(searchCue))
                    .collect(Collectors.toList());
            ObservableList<String> observableList  = FXCollections.observableList(filteredWords);
            wordList.setItems(observableList);
        }

    }

    @FXML
    protected void setFilterField(){

    }
}