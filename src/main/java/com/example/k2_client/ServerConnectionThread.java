package com.example.k2_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ServerConnectionThread extends Thread {
    Merger merger;

    public ServerConnectionThread(Merger mergerinto){
        merger = mergerinto;
    }

    public void run() {
        BufferedReader inputFromServer;
        List<String> words = new ArrayList<>();
        try {
            Socket socket = new Socket("localhost", 5000);
            inputFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String input =  LocalTime.now()
                    .truncatedTo(ChronoUnit.SECONDS)
                    .format(DateTimeFormatter.ISO_LOCAL_TIME) + " " + inputFromServer.readLine();
            words.add(input);
            System.out.println(input);
            merger.callSetWordCountLabel(words.size());
            merger.callSetWordList(words);
            while (input != null) {
                input =  LocalTime.now()
                        .truncatedTo(ChronoUnit.SECONDS)
                        .format(DateTimeFormatter.ISO_LOCAL_TIME)+ " " + inputFromServer.readLine();
                words.add(input);
                List<String> sortedWords = words
                        .stream()
                        .sorted(Comparator.comparing(strings -> strings.split (" ")[1]))
                        .collect(Collectors.toList());
                merger.callSetWordCountLabel(sortedWords.size());
                merger.callSetWordList(sortedWords);
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
}
