package com.example.k2_client;

import javafx.application.Platform;

import java.util.List;

public class Merger {
    HelloController helloController;

    public void setHelloController(HelloController helloControlleringer){
        helloController = helloControlleringer;
    }

    public void callSetWordCountLabel(int number){
        Platform.runLater (()->helloController.setWordCountLabel (number));
    }

    public void callSetWordList(List<String> list){
        Platform.runLater (()->helloController.setWordList (list));
    }
}
