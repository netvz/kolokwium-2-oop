package com.example.k2_client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        Merger merger = new Merger();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("view.fxml"));
        fxmlLoader.setControllerFactory (controllerClass -> new HelloController(merger));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("WordBag");
        stage.setScene(scene);
        stage.show();
        ServerConnectionThread serverConnectionThread = new ServerConnectionThread(merger);
        serverConnectionThread.start();
    }

    public static void main(String[] args) {
        launch();
    }
}