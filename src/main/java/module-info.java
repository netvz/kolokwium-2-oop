module com.example.k2_client {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.k2_client to javafx.fxml;
    exports com.example.k2_client;
}